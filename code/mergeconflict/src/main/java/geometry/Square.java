package geometry;

public class Square {
    //fields
    private int length;
    //Constructor
    public Square(int length) {
        this.length = length;
    }
    //Getter method
    public int getLength() {
        return this.length;
    }
    //Get area method
    public int getArea() {
        return length * length;
    }
    //toString method
    public String toString() {
        return "This is my length: " + getLength() + "\nThis is my area: " + getArea();
    }
}
